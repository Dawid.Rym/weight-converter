//Global variables
let kgInput = document.querySelector('#kgInput');
let gramsInput = document.querySelector('#gramsOutput');
let tonsInput = document.querySelector('#TOutput');
let ozInput = document.querySelector('#ozOutput');
let lbsInput = document.querySelector('#lbsOutput');

// zdarzenie oninput pojawia się natychmiast po zmianie wartości elementu/inputa
//input event  occurs immediately after the value of an element has changed
kgInput.addEventListener('input', convert)

function convert(e){
    let kilograms = e.target.value;
    
    gramsInput.innerHTML = kilograms * 1000; 
    tonsInput.innerHTML = kilograms / 1000;
    ozInput.innerHTML = kilograms * 35
    lbsInput.innerHTML = kilograms *2;
}


